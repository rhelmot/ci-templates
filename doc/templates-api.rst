.. _templates_api:

CI Templates API Reference
==========================

Alpine templates
----------------

.. autoyaml:: templates/alpine.yml

Arch templates
----------------

.. autoyaml:: templates/arch.yml

CentOS templates
----------------

.. autoyaml:: templates/centos.yml

Debian templates
----------------

.. autoyaml:: templates/debian.yml

Fedora templates
----------------

.. autoyaml:: templates/fedora.yml

openSUSE templates
----------------

.. autoyaml:: templates/opensuse.yml

Ubuntu templates
----------------

.. autoyaml:: templates/ubuntu.yml
